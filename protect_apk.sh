# !/bin/bash
cp app/build/outputs/apk/debug/app-debug.apk app/build/outputs/apk/debug/app-debug-${CI_PIPELINE_ID}.apk 
curl https://dev.protection-service.cloakware.irdeto.io/api/v1/requests -H "Authorization: Bearer ${TS_TOKEN}" -F "file=@app/build/outputs/apk/debug/app-debug-${CI_PIPELINE_ID}.apk" -F "securityLevel=4"